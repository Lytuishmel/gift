<?php

namespace Luxinten\FreeGift\Model;

use Luxinten\FreeGift\Api\ConfigInterface;
use Luxinten\FreeGift\Api\Data\FreeGiftOrderDatabaseInterface;
use Luxinten\FreeGift\Api\FreeGiftOrderInterface;
use Magento\Catalog\Model\ProductRepository;
use Magento\Sales\Model\Order;

class FreeGiftOrder implements FreeGiftOrderInterface
{
    private $config;
    private $productRepository;
    private $giftOrderModel;
    private $giftOrderItemModel;

    /**
     * FreeGiftOrder constructor.
     * @param ConfigInterface $config
     * @param \Luxinten\FreeGift\Model\FreeGiftOrderDatabase $freeGiftOrderDatabase
     * @param \Luxinten\FreeGift\Model\FreeGiftOrderItemDatabase $freeGiftOrderItemDatabase
     * @param ProductRepository $productRepository
     */
    public function __construct(
        ConfigInterface $config,
        FreeGiftOrderDatabase $freeGiftOrderDatabase,
        FreeGiftOrderItemDatabase $freeGiftOrderItemDatabase,
        ProductRepository $productRepository
    ) {
        $this->config = $config;
        $this->productRepository = $productRepository;
        $this->giftOrderModel = $freeGiftOrderDatabase;
        $this->giftOrderItemModel = $freeGiftOrderItemDatabase;
    }

    /**
     * @param $order
     *
     * @return null
     */
    public function execute($order)
    {
        /** @var  $order Order */
        $isEnabled = $this->config->isEnabled();
        $isLogEnabled = $this->config->isLogsEnabled();

        if ($isEnabled and $isLogEnabled) {
            $orderItems = $order->getAllItems();

            $giftsData = [];
            $giftsQty = 0;
            foreach ($orderItems as $item) {
                $product = $this->productRepository->getById($item->getProductId());
                $giftAttribute = $product->getCustomAttribute(ConfigInterface::XML_CONFIG_FREE_GIFT_ATTRIBUTE_CODE);
                if (true == is_object($giftAttribute) and true == $giftAttribute->getValue()) {
                    $giftsData[] = [
                        FreeGiftOrderItemDatabase::DATABASE_LUX_GIFT_ORDER_ITEM_GIFT_ORDER_ID => $order->getId(),
                        FreeGiftOrderItemDatabase::DATABASE_LUX_GIFT_ORDER_ITEM_PRODUCT_ID => $item->getProductId(),
                        FreeGiftOrderItemDatabase::DATABASE_LUX_GIFT_ORDER_ITEM_PRODUCT_NAME => $item->getName(),
                        FreeGiftOrderItemDatabase::DATABASE_LUX_GIFT_ORDER_ITEM_QTY => $item->getQtyOrdered()
                    ];

                    $giftsQty += $item->getQtyOrdered();
                }
            }

            if (!empty($giftsData)) {
                $orderData = [
                    FreeGiftOrderDatabaseInterface::DATABASE_LUX_GIFT_ORDER_ORDER_ID => $order->getId(),
                    FreeGiftOrderDatabaseInterface::DATABASE_LUX_GIFT_ORDER_ORDER_INCREMENT_ID => $order->getIncrementId(),
                    FreeGiftOrderDatabaseInterface::DATABASE_LUX_GIFT_ORDER_ORDER_TOTAL => $order->getSubtotal(),
                    FreeGiftOrderDatabaseInterface::DATABASE_LUX_GIFT_ORDER_GIFT_PRODUCT_QTY => $giftsQty
                ];

                $this->giftOrderModel->setData($orderData);

                try {
                    $this->giftOrderModel->save();
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage($e->getMessage());
                }

                foreach ($giftsData as $giftItem) {
                    $this->giftOrderItemModel->setData($giftItem);
                    try {
                        $this->giftOrderItemModel->save();
                    } catch (\Exception $e) {
                        $this->messageManager->addErrorMessage($e->getMessage());
                    }
                }
            }
        }
    }
}
