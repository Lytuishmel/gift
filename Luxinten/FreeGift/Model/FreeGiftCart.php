<?php

namespace Luxinten\FreeGift\Model;

use Luxinten\FreeGift\Api\ConfigInterface;
use Luxinten\FreeGift\Api\FreeGiftCartInterface;
use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Magento\Quote\Model\Quote;

class FreeGiftCart implements FreeGiftCartInterface
{

    protected $config;
    protected $_productRepository;
    protected $stockItemRepository;
    protected $_productCollectionFactory;

    public function __construct(
        ConfigInterface $config,
        ProductRepository $productRepository,
        StockItemRepository $stockItemRepository,
        CollectionFactory $productCollectionFactory,
        array $data = []
    ) {
        $this->config = $config;
        $this->_productRepository = $productRepository;
        $this->stockItemRepository = $stockItemRepository;
        $this->_productCollectionFactory = $productCollectionFactory;
    }

    /**
     * @param $quote
     * @return null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute($quote)
    {
        /** @var  $quote Quote */
        if ($this->config->isEnabled()) {
            $giftDemand = intval(floor($quote->getSubtotal() / $this->config->getMinimumOrderTotal()));
            $productGifts = $this->getGiftsList();
            $GiftItemsInQuote = intval($this->getGiftsInQuote($quote, $productGifts));

            if ($giftDemand !== $GiftItemsInQuote) {
                if ($this->config->isExtraGiftEnabled()) {
                    $this->updateGiftsInQuote($quote, $giftDemand, $productGifts);
                } else {
                    $this->updateGiftsInQuote($quote, $giftDemand > 0 ? 1 : 0, $productGifts);
                }
            }
        }
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getGiftsList()
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToFilter(ConfigInterface::XML_CONFIG_FREE_GIFT_ATTRIBUTE_CODE, true);

        $result = [];
        foreach ($collection as $product) {
            $result[] = $this->_productRepository->getById($product->getEntityId());
        }

        return $result;
    }

    /**
     * @param $quote
     * @param $productGifts
     * @return array
     */
    public function getGiftsInQuote($quote, $productGifts)
    {
        /** @var  $quote Quote */
        $result = 0;

        foreach ($productGifts as $product) {
            if ($quote->hasProductId($product->getID())) {
                $result += $quote->getItemByProduct($product)->getQty();
            }
        }
        return $result;
    }

    /**
     * @param $quote
     * @param $giftDemand
     * @param $productGifts
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function updateGiftsInQuote($quote, $giftDemand, $productGifts)
    {
        foreach ($productGifts as $product) {
            if ($quote->hasProductId($product->getID())) {
                $quote->removeItem($quote->getItemByProduct($product)->getItemId());
            }
        }

        foreach ($productGifts as $product) {
            $giftInStock[$product->getId()] = $this->stockItemRepository->get($product->getId())->getQty();
            $giftInQuote[$product->getId()] = 0;
        }

        $this->prepareGiftQuote($giftInStock, $giftInQuote, $giftDemand);

        foreach ($giftInQuote as $productId => $productQty) {
            if ($productQty > 0) {
                $quote->addProduct($this->_productRepository->getById($productId), $productQty);
            }
        }
    }

    /**
     * @param $giftInStock
     * @param $giftInQuote
     * @param $giftDemand
     */
    public function prepareGiftQuote(&$giftInStock, &$giftInQuote, &$giftDemand)
    {
        foreach ($giftInStock as $productId => &$productQty) {
            if ($productQty > 0 and $giftDemand > 0) {
                $giftInQuote[$productId] += 1;
                $productQty--;
                $giftDemand--;
            }
        }
        if ($giftDemand > 0 and array_sum($giftInStock) > 0) {
            $this->prepareGiftQuote($giftInStock, $giftInQuote, $giftDemand);
        }
    }
}
