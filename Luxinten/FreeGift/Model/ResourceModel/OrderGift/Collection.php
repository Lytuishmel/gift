<?php

namespace Luxinten\FreeGift\Model\ResourceModel\OrderGift;

use Luxinten\FreeGift\Model\FreeGiftOrderDatabase;
use Luxinten\FreeGift\Model\ResourceModel\OrderGift as ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(FreeGiftOrderDatabase::class, ResourceModel::class);
    }
}
