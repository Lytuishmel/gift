<?php

namespace Luxinten\FreeGift\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Luxinten\FreeGift\Api\Data\FreeGiftOrderDatabaseInterface;

class OrderGift extends AbstractDb
{

    protected function _construct()
    {
        $this->_init(FreeGiftOrderDatabaseInterface::DATABASE_TABLE_LUX_GIFT_ORDER, FreeGiftOrderDatabaseInterface::DATABASE_LUX_GIFT_ORDER_ID);
    }
}
