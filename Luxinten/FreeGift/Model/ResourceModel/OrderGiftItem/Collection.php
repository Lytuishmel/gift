<?php

namespace Luxinten\FreeGift\Model\ResourceModel\OrderGiftItem;

use Luxinten\FreeGift\Model\FreeGiftOrderItemDatabase;
use Luxinten\FreeGift\Model\ResourceModel\OrderGiftItem as ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(FreeGiftOrderItemDatabase::class, ResourceModel::class);
    }
}
