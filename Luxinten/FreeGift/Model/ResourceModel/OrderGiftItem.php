<?php

namespace Luxinten\FreeGift\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Luxinten\FreeGift\Api\Data\FreeGiftOrderItemDatabaseInterface;

class OrderGiftItem extends AbstractDb
{

    protected function _construct()
    {
        $this->_init(FreeGiftOrderItemDatabaseInterface::DATABASE_TABLE_LUX_GIFT_ORDER_ITEM, FreeGiftOrderItemDatabaseInterface::DATABASE_LUX_GIFT_ORDER_ITEM_ID);
    }
}
