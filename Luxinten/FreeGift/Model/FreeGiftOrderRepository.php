<?php

namespace Luxinten\FreeGift\Model;

use Luxinten\FreeGift\Api\Data\FreeGiftOrderDatabaseInterface;
use Luxinten\FreeGift\Api\Data\FreeGiftOrderDatabaseInterfaceFactory as FreeGiftOrderFactory;
use Luxinten\FreeGift\Api\FreeGiftOrderRepositoryInterface;
use Luxinten\FreeGift\Model\ResourceModel\OrderGift as ResourceModel;
use Luxinten\FreeGift\Model\ResourceModel\OrderGift\CollectionFactory;
use Magento\Framework\DB\Adapter\ConnectionException;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\ValidatorException;

class FreeGiftOrderRepository implements FreeGiftOrderRepositoryInterface
{
    private $resourceModel;

    private $collectionFactory;
    private $freeGiftOrderFactory;

    /**
     * FreeGiftOrderRepository constructor.
     * @param ResourceModel $resourceModel
     * @param CollectionFactory $collectionFactory
     * @param FreeGiftOrderFactory $freeGiftOrderFactory
     */
    public function __construct(
        ResourceModel $resourceModel,
        CollectionFactory $collectionFactory,
        FreeGiftOrderFactory $freeGiftOrderFactory
    ) {
        $this->resourceModel = $resourceModel;
        $this->collectionFactory = $collectionFactory;
        $this->freeGiftOrderFactory = $freeGiftOrderFactory;
    }

    /**
     * @param FreeGiftOrderDatabaseInterface $data
     * @throws CouldNotSaveException
     * @throws \Magento\Framework\Exception\TemporaryState\CouldNotSaveException
     */
    public function save(FreeGiftOrderDatabaseInterface $data)
    {
        if ($data) {
            try {
                $this->resourceModel->save($data);
            } catch (ConnectionException $exception) {
                throw new \Magento\Framework\Exception\TemporaryState\CouldNotSaveException(
                    __('Database connection error'),
                    $exception,
                    $exception->getCode()
                );
            } catch (CouldNotSaveException $e) {
                throw new CouldNotSaveException(__('Unable to save product'), $e);
            } catch (ValidatorException $e) {
                throw new CouldNotSaveException(__($e->getMessage()));
            } catch (AlreadyExistsException $e) {
                throw new CouldNotSaveException(__('Already exist'));
            } catch (\Exception $e) {
                throw new CouldNotSaveException(__('Something went wrong'));
            }
        }
    }

    /**
     * @param int $ItemId
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getById($ItemId)
    {
        $model = $this->freeGiftOrderFactory->create();
        $this->resourceModel->load($model, $ItemId);

        if (!$model->getId()) {
            throw new NoSuchEntityException(__('Requested tweet doesn\'t exist'));
        }
        return $model;
    }

    /**
     * @return FreeGiftOrderDatabaseInterface
     */
    public function getNewModel()
    {
        return $this->freeGiftOrderFactory->create();
    }
}
