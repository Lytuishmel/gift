<?php

namespace Luxinten\FreeGift\Model;

use Luxinten\FreeGift\Api\Data\FreeGiftOrderItemDatabaseInterface;
use Magento\Framework\Model\AbstractModel;
use Luxinten\FreeGift\Model\ResourceModel\OrderGiftItem as ResourceModel;

class FreeGiftOrderItemDatabase extends AbstractModel implements FreeGiftOrderItemDatabaseInterface
{

    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::DATABASE_LUX_GIFT_ORDER_ITEM_ID);
    }

    /**
     * @return int
     */
    public function getGiftOrderId()
    {
        return $this->getData(self::DATABASE_LUX_GIFT_ORDER_ITEM_GIFT_ORDER_ID);
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->getData(self::DATABASE_LUX_GIFT_ORDER_ITEM_PRODUCT_ID);
    }

    /**
     * @return float
     */
    public function getProductName()
    {
        return $this->getData(self::DATABASE_LUX_GIFT_ORDER_ITEM_PRODUCT_NAME);
    }

    /**
     * @return int
     */
    public function getQty()
    {
        return $this->getData(self::DATABASE_LUX_GIFT_ORDER_ITEM_QTY);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function setGiftOrderId($data)
    {
        return $this->setData(self::DATABASE_LUX_GIFT_ORDER_ITEM_GIFT_ORDER_ID, $data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function setProductId($data)
    {
        return $this->setData(self::DATABASE_LUX_GIFT_ORDER_ITEM_PRODUCT_ID, $data);
    }

    /**
     * @param $data
     * @return string
     */
    public function setProductName($data)
    {
        return $this->setData(self::DATABASE_LUX_GIFT_ORDER_ITEM_PRODUCT_NAME, $data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function setQty($data)
    {
        return $this->setData(self::DATABASE_LUX_GIFT_ORDER_ITEM_QTY, $data);
    }
}
