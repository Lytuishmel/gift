<?php

namespace Luxinten\FreeGift\Model;

use Luxinten\FreeGift\Api\Data\FreeGiftOrderDatabaseInterface;
use Magento\Framework\Model\AbstractModel;
use Luxinten\FreeGift\Model\ResourceModel\OrderGift as ResourceModel;

class FreeGiftOrderDatabase extends AbstractModel implements FreeGiftOrderDatabaseInterface
{

    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::DATABASE_LUX_GIFT_ORDER_ID);
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->getData(self::DATABASE_LUX_GIFT_ORDER_ORDER_ID);
    }

    /**
     * @return int
     */
    public function getOrderIncrementId()
    {
        return $this->getData(self::DATABASE_LUX_GIFT_ORDER_ORDER_INCREMENT_ID);
    }

    /**
     * @return float
     */
    public function getOrderTotal()
    {
        return $this->getData(self::DATABASE_LUX_GIFT_ORDER_ORDER_TOTAL);
    }

    /**
     * @return int
     */
    public function getGiftProductQty()
    {
        return $this->getData(self::DATABASE_LUX_GIFT_ORDER_GIFT_PRODUCT_QTY);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function setOrderId($data)
    {
        return $this->setData(self::DATABASE_LUX_GIFT_ORDER_ORDER_ID, $data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function setOrderIncrementId($data)
    {
        return $this->setData(self::DATABASE_LUX_GIFT_ORDER_ORDER_INCREMENT_ID, $data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function setOrderTotal($data)
    {
        return $this->setData(self::DATABASE_LUX_GIFT_ORDER_ORDER_TOTAL, $data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function setGiftProductQty($data)
    {
        return $this->setData(self::DATABASE_LUX_GIFT_ORDER_GIFT_PRODUCT_QTY, $data);
    }
}
