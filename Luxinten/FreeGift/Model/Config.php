<?php

namespace Luxinten\FreeGift\Model;

use Luxinten\FreeGift\Api\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Config
 * @package Luxinten\HazmatProducts\Model
 */
class Config implements ConfigInterface
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * Config constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_CONFIG_FREE_GIFT_ENABLED,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return int
     */
    public function getMinimumOrderTotal(): ?int
    {
        return $this->scopeConfig->getValue(
            self::XML_CONFIG_FREE_GIFT_ORDER_TOTAL,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return bool
     */
    public function isExtraGiftEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_CONFIG_EXTRA_FREE_GIFT_ENABLED,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return bool
     */
    public function isLogsEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_CONFIG_FREE_GIFT_LOGS_ENABLED,
            ScopeInterface::SCOPE_STORE
        );
    }

}
