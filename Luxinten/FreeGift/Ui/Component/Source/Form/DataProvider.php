<?php
namespace Luxinten\FreeGift\Ui\Component\Source\Form;

use Luxinten\FreeGift\Model\ResourceModel\OrderGift\CollectionFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{
    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        $result = [];
        $items  = $this->collection->getItems();

        foreach ($items as $tabItem) {
            $result[$tabItem->getId()] = $tabItem->getData();
        }

        return $result;
    }
}
