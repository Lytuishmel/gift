<?php

namespace Luxinten\FreeGift\Block\Adminhtml\GiftOrderItem\Edit;

use Luxinten\FreeGift\Model\FreeGiftOrderItemDatabase;

class GenericButton
{
    protected $urlBuilder;

    protected $registry;

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->registry = $registry;
    }

    public function getId()
    {
        /** @var FreeGiftOrderItemDatabase $orderItemGift */
        $orderItemGift = $this->registry->registry('orderGiftItem');

        return $orderItemGift ? $orderItemGift->getId() : null;
    }

    public function getUrl($route='', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }
}
