<?php

namespace Luxinten\FreeGift\Block\Adminhtml\GiftOrderItem\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class ResetButton implements ButtonProviderInterface
{

    /**
     * Retrieve button-specified settings
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' =>__('Reset'),
            'class' => 'reset',
            'on_click' => 'location.reload(true);',
            'sort_order' => 30
        ];
    }
}
