<?php

namespace Luxinten\FreeGift\Block\Adminhtml\GiftOrder\Edit;


use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class SaveButton extends GenericButton implements ButtonProviderInterface
{

    /**
     * Retrieve button-specified settings
     *
     * @return array
     */
    public function getButtonData()
    {
        return  [
            'label' => __("Save Data"),
            'class' => 'save primary',
            'sort_order' => 50
        ];

    }
}
