<?php

namespace Luxinten\FreeGift\Block\Adminhtml\GiftOrder\Edit;

use Luxinten\FreeGift\Model\FreeGiftOrderDatabase;

class GenericButton
{
    protected $urlBuilder;

    protected $registry;

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->registry = $registry;
    }

    public function getId()
    {
        /** @var FreeGiftOrderDatabase $orderGift */
        $orderGift = $this->registry->registry('orderGift');

        return $orderGift ? $orderGift->getId() : null;
    }

    public function getUrl($route='', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }
}
