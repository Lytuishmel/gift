<?php

namespace Luxinten\FreeGift\Plugin\Magento\Quote\Model;

use Luxinten\FreeGift\Model\FreeGiftCart;
use Magento\Quote\Model\Quote;

class AfterSaveCart
{
    private $freeGiftCart;

    /**
     * @param FreeGiftCart $freeGiftCart
     */
    public function __construct(
        FreeGiftCart $freeGiftCart
    ) {
        $this->freeGiftCart = $freeGiftCart;
    }

    /**
     * @param Quote $subject
     */
    public function afterafterSave(
        Quote $subject
    ) {

        $this->freeGiftCart->execute($subject);
    }
}
