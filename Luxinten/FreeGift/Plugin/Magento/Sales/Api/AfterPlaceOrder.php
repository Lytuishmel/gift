<?php

namespace Luxinten\FreeGift\Plugin\Magento\Sales\Api;

use Luxinten\FreeGift\Model\FreeGiftOrder;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderManagementInterface;

class AfterPlaceOrder
{
    private $freeGiftOrder;

    /**
     * @param FreeGiftOrder $freeGiftOrder
     */
    public function __construct(
        FreeGiftOrder $freeGiftOrder
    ) {
        $this->freeGiftOrder = $freeGiftOrder;
    }

    /**
     * @param OrderManagementInterface $subject
     * @param OrderInterface $order
     * @param OrderInterface $result
     * @return OrderInterface
     */
    public function afterPlace(
        OrderManagementInterface $subject,
        OrderInterface $order,
        OrderInterface $result
    ) {
        $this->freeGiftOrder->execute($order);

        return $order;
    }
}
