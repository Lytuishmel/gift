<?php

namespace Luxinten\FreeGift\Controller\Adminhtml\Gift;

use Luxinten\FreeGift\Api\FreeGiftOrderRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\Result\JsonFactory;

class InlineEdit extends Action
{
    protected $jsonFactory;
    protected $repository;

    public function __construct(
        FreeGiftOrderRepositoryInterface $repository,
        JsonFactory $jsonFactory,
        Action\Context $context
    ) {
        $this->repository = $repository;
        $this->jsonFactory = $jsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $message = [];

        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);

            if (!count($postItems)) {
                $message[] = __('Please correct data sent');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $modelId) {
                    $model = $this->repository->getById($modelId);
                    try {
                        $model->setData(array_merge($model->getData(), $postItems[$modelId]));
                        $this->repository->save($model);
                    } catch (\Exception $e) {
                        $message[] = $e->getMessage();
                        $error = true;
                    }
                }
            }
        }
        return $resultJson->setData([
            'messages' => $message,
            'error'    => $error
        ]);
    }
}
