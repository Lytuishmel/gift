<?php
namespace Luxinten\FreeGift\Controller\Adminhtml\Gift;

use Luxinten\FreeGift\Model\FreeGiftOrderDatabase;
use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Edit extends Action
{
    protected $freeGiftOrderDatabaseModel;
    protected $pageFactory;
    protected $registry;

    public function __construct(
        PageFactory $pageFactory,
        FreeGiftOrderDatabase $freeGiftOrderDatabaseModel,
        Registry $registry,
        Action\Context $context
    ) {
        $this->freeGiftOrderDatabaseModel = $freeGiftOrderDatabaseModel;
        $this->pageFactory = $pageFactory;
        $this->registry = $registry;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Luxinten_FreeGift::order_list');
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->freeGiftOrderDatabaseModel;

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This order does not exists'));

                $result = $this->resultRedirectFactory->create();
                return $result->setPath("order/gift/index");
            }
        }

        $data = $this->_getSession()->getFormData(true);

        if (!empty($data)) {
            $model->setData($data);
        }

        $this->registry->register('orderGift', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->pageFactory->create();

        $resultPage->addBreadcrumb(
            $id ? __('Edit Order') : __('Add a New Order'),
            $id ? __('Edit Order') : __('Add a New Order')
        );
        if ($id) {
            $resultPage->getConfig()->getTitle()->prepend('Edit');
        } else {
            $resultPage->getConfig()->getTitle()->prepend('Add');
        }
        return $this->pageFactory->create();
    }
}
