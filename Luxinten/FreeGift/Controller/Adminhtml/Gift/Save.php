<?php

namespace Luxinten\FreeGift\Controller\Adminhtml\Gift;

use Luxinten\FreeGift\Model\FreeGiftOrderDatabase;
use Luxinten\FreeGift\Api\FreeGiftOrderRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\View\Result\PageFactory;

class Save extends Action
{
    private $model;
    private $pageFactory;
    protected $resultRedirectFactory;
    private $repository;

    public function __construct(
        RedirectFactory $redirectFactory,
        FreeGiftOrderDatabase $freeGiftOrderDatabase,
        FreeGiftOrderRepositoryInterface $repository,
        PageFactory $pageFactory,
        Action\Context $context
    ) {
        $this->resultRedirectFactory = $redirectFactory;
        $this->model = $freeGiftOrderDatabase;
        $this->repository = $repository;
        $this->pageFactory = $pageFactory;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Luxinten_FreeGift::order_list');
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($data) {
            if (empty($data['id'])) {
                $data['id'] = null;
                $model = $this->repository->getNewModel();
            } else {
                $model = $this->repository->getById($data['id']);
            }

            try {
                $model->setData($data);
                $this->repository->save($model);
                $this->messageManager->addSuccessMessage(__('Gift Order Saved Successfully'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' =>$model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/index');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
            return $resultRedirect->setPath('*/*/index');
        }

        return $resultRedirect->setPath('*/*/');
    }
}
