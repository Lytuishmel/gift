<?php

namespace Luxinten\FreeGift\Controller\Adminhtml\Gift;

use Luxinten\FreeGift\Api\FreeGiftOrderRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\View\Result\PageFactory;


class Delete extends Action
{
    private $pageFactory;
    protected $resultRedirectFactory;
    private $repository;

    public function __construct(
        RedirectFactory $redirectFactory,
        FreeGiftOrderRepositoryInterface $repository,
        PageFactory $pageFactory,
        Action\Context $context
    ) {
        $this->resultRedirectFactory = $redirectFactory;
        $this->repository = $repository;
        $this->pageFactory = $pageFactory;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Luxinten_FreeGift::order_list');
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $model = $this->repository->getById($id);
            try {
                $model->delete();
                $this->messageManager->addSuccessMessage(__('Gift Order Deleted'));
                return $resultRedirect->setPath('*/*/index');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__($e->getMessage()));
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }

        return $resultRedirect->setPath('*/*/index');
    }
}
