<?php

namespace Luxinten\FreeGift\Controller\Adminhtml\GiftItem;

use Luxinten\FreeGift\Model\FreeGiftOrderItemDatabase;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\View\Result\PageFactory;

class Delete extends Action
{
    protected $model;
    private $pageFactory;
    protected $resultRedirectFactory;
    public function __construct(
        RedirectFactory $redirectFactory,
        FreeGiftOrderItemDatabase $freeGiftOrderItemDatabase,
        PageFactory $pageFactory,
        Action\Context $context
    ) {
        $this->resultRedirectFactory = $redirectFactory;
        $this->model = $freeGiftOrderItemDatabase;
        $this->pageFactory = $pageFactory;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Luxinten_FreeGift::order_item_list');
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $model = $this->model;
            $model->load($id);
            try {
                $model->delete();
                $this->messageManager->addSuccessMessage(__('Gift Item Deleted'));
                return $resultRedirect->setPath('*/*/index');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__($e->getMessage()));
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }

        return $resultRedirect->setPath('*/*/index');
    }
}
