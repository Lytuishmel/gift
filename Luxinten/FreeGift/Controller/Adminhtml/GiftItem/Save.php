<?php

namespace Luxinten\FreeGift\Controller\Adminhtml\GiftItem;

use Luxinten\FreeGift\Model\FreeGiftOrderItemDatabase;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\View\Result\PageFactory;

class Save extends Action
{
    protected $model;
    protected $pageFactory;
    protected $resultRedirectFactory;

    public function __construct(
        RedirectFactory $redirectFactory,
        FreeGiftOrderItemDatabase $freeGiftOrderItemDatabase,
        PageFactory $pageFactory,
        Action\Context $context
    ) {
        $this->resultRedirectFactory = $redirectFactory;
        $this->model = $freeGiftOrderItemDatabase;
        $this->pageFactory = $pageFactory;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Luxinten_FreeGift::order_item_list');
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($data) {
            if (empty($data['id'])) {
                $data['id'] = null;
                $model = $this->model->setData($data);
            } else {
                $model = $this->model->load($data['id']);
                $model->setData($data);
            }

            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('Gift Order Item Saved Successfully'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' =>$model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/index');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
            return $resultRedirect->setPath('*/*/index');
        }

        return $resultRedirect->setPath('*/*/');
    }
}
