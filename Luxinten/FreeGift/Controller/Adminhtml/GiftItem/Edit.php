<?php
namespace Luxinten\FreeGift\Controller\Adminhtml\GiftItem;

use Luxinten\FreeGift\Model\FreeGiftOrderItemDatabase;
use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Edit extends Action
{
    protected $freeGiftOrderItemDatabase;
    protected $pageFactory;
    protected $registry;

    public function __construct(
        PageFactory $pageFactory,
        FreeGiftOrderItemDatabase $freeGiftOrderItemDatabase,
        Registry $registry,
        Action\Context $context
    ) {
        $this->freeGiftOrderItemDatabase = $freeGiftOrderItemDatabase;
        $this->pageFactory = $pageFactory;
        $this->registry = $registry;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Luxinten_FreeGift::order_item_list');
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->freeGiftOrderItemDatabase;

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This order item does not exists'));

                $result = $this->resultRedirectFactory->create();
                return $result->setPath("order/giftitem/index");
            }
        }

        $data = $this->_getSession()->getFormData(true);

        if (!empty($data)) {
            $model->setData($data);
        }

        $this->registry->register('orderGiftItem', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->pageFactory->create();

        $resultPage->addBreadcrumb(
            $id ? __('Edit Item') : __('Add a New Item'),
            $id ? __('Edit Item') : __('Add a New Item')
        );
        if ($id) {
            $resultPage->getConfig()->getTitle()->prepend('Edit');
        } else {
            $resultPage->getConfig()->getTitle()->prepend('Add');
        }
        return $this->pageFactory->create();
    }
}
