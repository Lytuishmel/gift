<?php

namespace Luxinten\FreeGift\Controller\Adminhtml\GiftItem;

use Luxinten\FreeGift\Model\FreeGiftOrderItemDatabase;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\Result\JsonFactory;

class InlineEdit extends Action
{
    protected $freeGiftOrderItemDatabase;
    protected $jsonFactory;

    public function __construct(
        FreeGiftOrderItemDatabase $freeGiftOrderItemDatabase,
        JsonFactory $jsonFactory,
        Action\Context $context
    ) {
        $this->freeGiftOrderItemDatabase = $freeGiftOrderItemDatabase;
        $this->jsonFactory = $jsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $message = [];

        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);

            if (!count($postItems)) {
                $message[] = __('Please correct data sent');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $modelId) {
                    $model = $this->freeGiftOrderItemDatabase->load($modelId);
                    try {
                        $model->setData(array_merge($model->getData(), $postItems[$modelId]));
                        $model->save();
                    } catch (\Exception $e) {
                        $message[] = $e->getMessage();
                        $error = true;
                    }
                }
            }
        }
        return $resultJson->setData([
            'messages' => $message,
            'error'    => $error
        ]);
    }
}
