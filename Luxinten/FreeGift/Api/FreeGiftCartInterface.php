<?php


namespace Luxinten\FreeGift\Api;


interface FreeGiftCartInterface
{
    /**
     * @param $cart
     *
     * @return null
     */
    public function execute($cart);

    /**
     * @return array
     */
    public function getGiftsList();

    /**
     * @param $quote
     * @param $gifts
     * @return array
     */
    public function getGiftsInQuote($quote, $gifts);

    /**
     * @param $quote
     * @param $giftDemand
     * @param $productGifts
     * @return mixed
     */
    public function updateGiftsInQuote($quote, $giftDemand, $productGifts);

    /**
     * @param $giftInStock
     * @param $giftInQuote
     * @param $giftDemand
     */
    public function prepareGiftQuote(&$giftInStock, &$giftInQuote, &$giftDemand);
}
