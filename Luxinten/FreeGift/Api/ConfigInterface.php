<?php

namespace Luxinten\FreeGift\Api;

interface ConfigInterface
{
    const XML_CONFIG_FREE_GIFT_ENABLED = 'free_gift/free_gift_config/free_gift_status';
    const XML_CONFIG_FREE_GIFT_ORDER_TOTAL = 'free_gift/free_gift_config/free_gift_total';
    const XML_CONFIG_EXTRA_FREE_GIFT_ENABLED = 'free_gift/free_gift_config/extra_gift';
    const XML_CONFIG_FREE_GIFT_LOGS_ENABLED = 'free_gift/free_gift_config/free_gift_logs';
    const XML_CONFIG_FREE_GIFT_ATTRIBUTE_CODE = 'free_gift';
    const XML_CONFIG_FREE_GIFT_ATTRIBUTE_LABEL = 'Free Gift';

    /**
     * @return bool
     */
    public function isEnabled(): bool;

    /**
     * @return int|null
     */
    public function getMinimumOrderTotal(): ?int;

    /**
     * @return bool
     */
    public function isExtraGiftEnabled(): bool;

    /**
     * @return bool
     */
    public function isLogsEnabled(): bool;

}
