<?php


namespace Luxinten\FreeGift\Api;


interface FreeGiftOrderInterface
{
    /**
     * @param $order
     *
     * @return null
     */
    public function execute($order);

}
