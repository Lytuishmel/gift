<?php

namespace Luxinten\FreeGift\Api\Data;

interface FreeGiftOrderItemDatabaseInterface
{
    const DATABASE_TABLE_LUX_GIFT_ORDER_ITEM         = 'lux_gift_order_item';

    const DATABASE_LUX_GIFT_ORDER_ITEM_ID            = 'id';
    const DATABASE_LUX_GIFT_ORDER_ITEM_GIFT_ORDER_ID = 'gift_order_id';
    const DATABASE_LUX_GIFT_ORDER_ITEM_PRODUCT_ID    = 'product_id';
    const DATABASE_LUX_GIFT_ORDER_ITEM_PRODUCT_NAME  = 'product_name';
    const DATABASE_LUX_GIFT_ORDER_ITEM_QTY           = 'qty';

    /**
     * @return int
     */
    public function getId();

    /**
     * @return int
     */
    public function getGiftOrderId();

    /**
     * @return int
     */
    public function getProductId();

    /**
     * @return string
     */
    public function getProductName();

    /**
     * @return int
     */
    public function getQty();

    /**
     * @param $data
     * @return mixed
     */
    public function setGiftOrderId($data);

    /**
     * @param $data
     * @return mixed
     */
    public function setProductId($data);

    /**
     * @param $data
     * @return mixed
     */
    public function setProductName($data);

    /**
     * @param $data
     * @return mixed
     */
    public function setQty($data);

}
