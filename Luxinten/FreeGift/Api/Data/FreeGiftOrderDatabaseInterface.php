<?php

namespace Luxinten\FreeGift\Api\Data;

interface FreeGiftOrderDatabaseInterface
{
    const DATABASE_TABLE_LUX_GIFT_ORDER              = 'lux_gift_order';

    const DATABASE_LUX_GIFT_ORDER_ID                 = 'id';
    const DATABASE_LUX_GIFT_ORDER_ORDER_ID           = 'order_id';
    const DATABASE_LUX_GIFT_ORDER_ORDER_INCREMENT_ID = 'order_increment_id';
    const DATABASE_LUX_GIFT_ORDER_ORDER_TOTAL        = 'order_total';
    const DATABASE_LUX_GIFT_ORDER_GIFT_PRODUCT_QTY   = 'gift_product_qty';

    /**
     * @return int
     */
    public function getId();

    /**
     * @return int
     */
    public function getOrderId();

    /**
     * @return int
     */
    public function getOrderIncrementId();

    /**
     * @return float
     */
    public function getOrderTotal();

    /**
     * @return int
     */
    public function getGiftProductQty();

    /**
     * @param $data
     * @return mixed
     */
    public function setOrderId($data);

    /**
     * @param $data
     * @return mixed
     */
    public function setOrderIncrementId($data);

    /**
     * @param $data
     * @return mixed
     */
    public function setOrderTotal($data);

    /**
     * @param $data
     * @return mixed
     */
    public function setGiftProductQty($data);

}
