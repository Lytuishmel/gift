<?php

namespace Luxinten\FreeGift\Api;

use Luxinten\FreeGift\Api\Data\FreeGiftOrderDatabaseInterface;

interface FreeGiftOrderRepositoryInterface
{

    /**
     * save firstTest model
     * @param FreeGiftOrderDatabaseInterface $data
     *
     * @return mixed
     */
    public function save(FreeGiftOrderDatabaseInterface $data);

    /**
     * @param int $ItemId
     *
     * @return mixed
     */
    public function getById($ItemId);

    /**
     * @return mixed
     */
    public function getNewModel();
}
